import { db, auth } from './shared';
import { DataSnapshot } from 'firebase-functions/lib/providers/database';

export async function onUserDataRequest(userUid: string) {

    const stored_data = await requestUserData(userUid);
    const user = await auth.getUser(userUid);

    return { user, ...stored_data };
};

export function requestUserData(userUid: string) {

    const stored_data = {};

    return new Promise(resolve => {
        db.ref('/').on("value", (snap: DataSnapshot) => {

            snap.forEach(( subSnap: DataSnapshot ) => {

                if ( subSnap.hasChildren() ) {
                    subSnap.forEach((data: DataSnapshot) => data.key === userUid ? stored_data[subSnap.key] = data.val() : null);
                }

                return true;
            });

            resolve(stored_data);
        });
    });

}

