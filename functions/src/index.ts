import * as functions from 'firebase-functions';

import * as express from 'express';
import * as cors from 'cors';
import * as cookieParser from 'cookie-parser';

import { deleteAccounts, requestDeletionAPIFunction, cancelDeletionAPIFunction } from './account-deletion';
import * as AuthorizedOnlyMiddleware from './authorized-only';

const cookieParserImp = cookieParser();
const corsImp = cors({ origin: true });
const app = express();


  app.use(corsImp);
  app.use(cookieParserImp);
  app.use('/users/authenticated', AuthorizedOnlyMiddleware.authorizedFirebaseUser);

  app.post('/users/authenticated/request-deletion', requestDeletionAPIFunction);

  app.post('/users/request-deletion', requestDeletionAPIFunction);

  app.post('/users/cancel-deletion', cancelDeletionAPIFunction);

 exports.app = functions.https.onRequest(app);

 exports.hourly_job =
  functions.pubsub.topic('hourly-tick').onPublish((event) => {
    console.log("This job is ran every hour!");

    deleteAccounts();
  });