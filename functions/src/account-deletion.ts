import { DataSnapshot } from 'firebase-functions/lib/providers/database';
import { UserRecord } from 'firebase-functions/lib/providers/auth';

import { auth, db, generalError } from './shared';

interface AccountDeletionRequestItem {
    requestedOn: number;
    expiresOn: number;
}

const AnonymizedUser: Partial<UserRecord> = {
    disabled: true,
    email: 'anon@example.com',
    phoneNumber: null,
    emailVerified: false,
    displayName: 'AnonymizedUser',
    photoURL: '',
    metadata: null,
    customClaims: null
};

export function requestAccountDeletion(accountRef: string, requestedOn: number): Promise<AccountDeletionRequestItem> {
    const dateRequestedOn = new Date(requestedOn),
        dateExpiresOn = new Date().setDate(dateRequestedOn.getDate() + 30);

    return db.ref(`/accountDeletionRequests/${accountRef}`).set({
        requestedOn: dateRequestedOn.valueOf(),
        expiresOn: dateExpiresOn.valueOf()
    }).then(() => ({
        requestedOn: dateRequestedOn.valueOf(),
        expiresOn: dateExpiresOn.valueOf()
    }));
}

export function cancelAccountDeletion(accountRef: string) {
    return db.ref(`/accountDeletionRequests/${accountRef}`).set(null);
}

export function checkUserCanLogin(accountRef: string) {
    return new Promise((resolve) => {
        db.ref(`/accountDeletionRequests/${accountRef}`)
            .on("value", (snapshot: DataSnapshot) => {
                resolve(!snapshot.exists());
            }, (error) => {
                console.log(error);
                resolve(false);
            });
    });
}

export function deleteAccounts() {
    db.ref('/accountDeletionRequests').on("value", (snapshot: DataSnapshot) => {
        if (snapshot.hasChildren()) {
            snapshot.forEach((a: DataSnapshot) => {
                if (a.exists()) {
                    const data = a.val() as AccountDeletionRequestItem;
                    const userRef = a.key;
                    const shouldBeDeleted = hasBeenThirtyDays(data.requestedOn, data.expiresOn);

                    console.log(' UID | Requested At | Expires on | Should be deleted this run? ')
                    console.log(` ${userRef} | ${data.expiresOn} | ${data.requestedOn} | ${shouldBeDeleted} `);

                    if (shouldBeDeleted) {
                        auth.updateUser(userRef, AnonymizedUser).catch((e) => { console.error(e) });
                    }
                }

                return true;

            });
        }
    });
}

function hasBeenThirtyDays(startedAt: number, endedAt: number) {
    return endedAt - startedAt >= 2.592e+9;
}

export const requestDeletionAPIFunction = (request, response) => {

    let date, uid;

    if (request.body.date) {
        date = request.body.date;
    } else {
        date = Date.now();
    }

    if (request.body.uid) {
        uid = request.body.uid;
    } else {
        uid = request.user.uid;
    }

    if (!uid) {
        response.status(400).send({ message: 'Missing the Uid, this is required to identify the user.' });
        return;
    }

    requestAccountDeletion(uid, date)
        .then((data) => {
            response.send({
                message: `your account will be deleted on ${data.expiresOn}, if you wish to cancel this, log in to the app before ${data.expiresOn}`,
                success: true
            });
        })
        .catch((e) => generalError(e, response));
}

export const cancelDeletionAPIFunction = (request, response) => {

    const userEmail = request.body.email;

    if (!userEmail) {
        response.status(400)
            .send({ error: "Invalid Request: No Email Address, this is required." });
        return;
    }

    auth.getUserByEmail(userEmail).then((userRecord) => cancelAccountDeletion(userRecord.uid))
        .then(() => response.send({ message: `You have cancelled your account deletion`, success: true }))
        .catch((e) => generalError(e, response));
};