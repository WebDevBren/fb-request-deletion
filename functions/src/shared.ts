import * as admin from 'firebase-admin';

import * as express from 'express';

export const firebase = admin.initializeApp();
export const auth = admin.auth();
export const db = admin.database();
export const firestore = admin.firestore();
export const messaging = admin.messaging();

export function generalError(error: Error, response: express.Response) {
    response.status(500).send({ error: error.message });
}
